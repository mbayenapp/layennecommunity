package app.mbayenn.com.layene_community.Activity;

import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Adapter.Comments_Adapter;
import app.mbayenn.com.layene_community.Helper.SessionManagerUser;
import app.mbayenn.com.layene_community.Model.Commentaires;
import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;

public class Comment extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private ListView lst_commentaires;
    private Comments_Adapter adapter;
    private List<Commentaires> commentsList;
    private EditText edit_sect_comment;
    private ImageView img_send_comment;
    private SessionManagerUser sessionManagerUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        final Bundle extras = getIntent().getExtras();
        final int id_article = extras.getInt("id_article");
        /*=====================================================*/
        toolbar = (Toolbar) findViewById(R.id.toolbar_comments);
        toolbar.setTitle("Commentaires de l'article");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*=====================================================*/
        sessionManagerUser = new SessionManagerUser(Comment.this);
        lst_commentaires = (ListView) findViewById(R.id.lst_liste_commentaire);
        edit_sect_comment = (EditText) findViewById(R.id.edit_sect_comment);
        img_send_comment = (ImageView) findViewById(R.id.img_send_comment);
        /*=====================================================*/
        commentsList = get_comments(id_article);
        adapter = new Comments_Adapter(Comment.this, commentsList);
        new CountDownTimer(1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                lst_commentaires.setAdapter(adapter);
            }
        }.start();
        lst_commentaires.setAdapter(adapter);
        img_send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = edit_sect_comment.getText().toString().trim();
                if (content.equals("")) {
                    Toast.makeText(getApplicationContext(), "Entrer votre commentaire", Toast.LENGTH_LONG).show();
                } else {
                    if (sessionManagerUser.isLoggedIn()) {
                        Users u = get_user(sessionManagerUser.getLoggedInUser().getEmail());
                        addCommentaire(content, u.getEmail(), u.getPrenom(), u.getNom(), id_article);
                    }/*else if (user != null){
                        String[] part = user.getDisplayName().split(" ");
                        addCommentaire(content, user.getEmail(), part[0], part[1], id_article);
                    }*/
                    commentsList.clear();
                    commentsList.addAll(get_comments(id_article));
                    adapter.notifyDataSetChanged();
                    edit_sect_comment.setText("");
                }
            }
        });

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
        }
        return false;
    }

    private List<Commentaires> get_comments(int id_article) {
        List<Commentaires> comments = new ArrayList<>();
        Commentaires comment;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result = "";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Articles/list_comments.php?id_article=" + id_article+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(), "UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    comment = new Commentaires();
                    comment.setId(jsonObject.optInt("id"));
                    comment.setDate_comment(jsonObject.optString("date_comment"));
                    comment.setContent(jsonObject.optString("content"));
                    comment.setEmail_user(jsonObject.optString("email_user"));
                    comment.setFirst_name(jsonObject.optString("first_name"));
                    comment.setLast_name(jsonObject.optString("last_name"));
                    comment.setId_article(jsonObject.optInt("id_article"));
                    comments.add(comment);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return comments;
    }

    /*Ajouter une commentaire*/
    private void addCommentaire(String content, String email_user, String first_name, String last_name, int id_article) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result = "";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Articles/add_comment.php?content=" +
                URLEncoder.encode(content) + "&email_user=" + email_user + "&first_name=" + first_name + "&last_name=" + last_name + "&id_article=" + id_article+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(), "UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
        return super.onOptionsItemSelected(item);
    }
    private Users get_user(String email){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/get_user.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setIndicatif(jsonObject.optString("indicatif"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}