package app.mbayenn.com.layene_community.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Adapter.Video_Similaire_Adapter;
import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

public class Start_Video extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private String token = "layenne2017";
    private Toolbar toolbar;
    public static final String API_KEY = "AIzaSyDXhIfMBIKLGNyxdlOLODPIl80xZA_Qmzs";
    String VIDEO_ID = null;
    private TextView txt_titre,txt_categorie;
    private ListView lst_videos;
    private Video_Similaire_Adapter adapter;
    private ImageView img_share_video;
    private ConnexionInternet connexionInternet;
    private ImageView icon_down,icon_arrow_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start__video);
        /*==========================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(Start_Video.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Start_Video.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            final Bundle extras = getIntent().getExtras();
            int id = extras.getInt("id");
            final String titre = extras.getString("titre");
            final String description = extras.getString("description");
            final String categorie = extras.getString("categorie");
            final String id_video = extras.getString("id_video");
            VIDEO_ID = id_video;
           /*Initialisation de youtube player*/
            YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_video_ID);
            youTubePlayerView.initialize(API_KEY, this);
            txt_titre = (TextView) findViewById(R.id.txt_titre_video_start);
            txt_categorie = (TextView) findViewById(R.id.txt_categorie_video_start);
            lst_videos = (ListView) findViewById(R.id.lst_videos_similaires);
            img_share_video = (ImageView) findViewById(R.id.img_share_video);
            icon_down = (ImageView)findViewById(R.id.icon_arrow_down);
            icon_arrow_up = (ImageView)findViewById(R.id.icon_arrow_up);
        /*==========================================================*/
        icon_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_titre.setMaxLines(6);
                icon_down.setVisibility(View.GONE);
                icon_arrow_up.setVisibility(View.VISIBLE);
            }
        });
            icon_arrow_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    icon_down.setVisibility(View.VISIBLE);
                    icon_arrow_up.setVisibility(View.GONE);
                    txt_titre.setMaxLines(2);
                }
            });
            txt_titre.setText(titre);
            txt_categorie.setText(categorie);
            final List<Videos> videos = get_Videos(id, categorie);
            adapter = new Video_Similaire_Adapter(Start_Video.this, videos);
            lst_videos.setAdapter(adapter);
            setListViewHeightBasedOnChildren(lst_videos);
            lst_videos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Videos video = videos.get(position);
                    Intent intent = new Intent(getApplicationContext(), Start_Video.class);
                    intent.putExtra("id", video.getId());
                    intent.putExtra("titre", video.getTitre());
                    intent.putExtra("description", video.getDescription());
                    intent.putExtra("categorie", video.getCategorie());
                    intent.putExtra("id_video", video.getId_video());
                    startActivity(intent);
                    finish();
                }
            });
            img_share_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text that will be shared.</p>"));
                startActivity(Intent.createChooser(sharingIntent,"Share using"));*/
                /*Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                String path = "https://www.youtube.com/watch?v="+id_video;
                Uri screenshotUri = Uri.parse(path);
                sharingIntent.setType("image/png");
                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                startActivity(Intent.createChooser(sharingIntent, "Partager la vidéo"));*/
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, titre);
                    share.putExtra(Intent.EXTRA_TEXT, "https://www.youtube.com/watch?v=" + id_video);
                    startActivity(Intent.createChooser(share, "Partager cette vidéo!"));
                }
            });
        /*==========================================================*/
        }
    }

    //Modifier la hauteur de mon listView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
            //player.cuePlaylist(VIDEO_ID);
            youTubePlayer.loadVideo(VIDEO_ID);
            //youTubePlayer.setFullscreen(true);
            youTubePlayer.play();
        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Erreur d'initialisation de la vidéo!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
    /*==========================================================*/
    //La méthode qui retourne la liste des vidéos
    private List<Videos> get_Videos(int id,String categorie){
        List<Videos> videos = new ArrayList<>();
        Videos video;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/videos/categorie_video.php?id="+id+"&categorie="+ URLEncoder.encode(categorie)+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    video = new Videos();
                    video.setId(jsonObject.optInt("id"));
                    video.setImage(jsonObject.optString("image"));
                    video.setTitre(jsonObject.optString("titre"));
                    video.setDescription(jsonObject.optString("description"));
                    video.setCategorie(jsonObject.optString("categorie"));
                    video.setId_video(jsonObject.getString("id_video"));
                    videos.add(video);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return videos;
    }
}
