package app.mbayenn.com.layene_community.Model;

/**
 * Created by bfali on 16/03/2017.
 */

public class Compagnons {
    private int id;
    private String nom,description,photo;

    public Compagnons() {
    }

    public Compagnons(int id, String nom, String description, String photo) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
