package app.mbayenn.com.layene_community.Activity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Adapter.Rubrique_Adapter;
import app.mbayenn.com.layene_community.Model.Rubriques;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

public class Start_Directe extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private String token = "layenne2017";
    private RecyclerView r_rubrique;
    private Rubrique_Adapter adapter;
    public static final String API_KEY = "AIzaSyDXhIfMBIKLGNyxdlOLODPIl80xZA_Qmzs";
    String VIDEO_ID = null;
    private ConnexionInternet connexionInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start__directe);
         /*========================================================*/
        r_rubrique = (RecyclerView)findViewById(R.id.rubrique);
        /*========================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(Start_Directe.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Start_Directe.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
           /*Initialisation de youtube player*/
            final Bundle extras = getIntent().getExtras();
            final String id_video = extras.getString("id_video");
            VIDEO_ID = id_video;
            YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_video_directe_ID);
            youTubePlayerView.initialize(API_KEY, this);
            List<Rubriques> rubriques = get_Rubriques();
            adapter = new Rubrique_Adapter(Start_Directe.this, rubriques);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
            r_rubrique.setLayoutManager(mLayoutManager);
            r_rubrique.addItemDecoration(new Start_Directe.GridSpacingItemDecoration(2, dpToPx(10), true));
            r_rubrique.setItemAnimator(new DefaultItemAnimator());
            r_rubrique.setAdapter(adapter);
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
            //player.cuePlaylist(VIDEO_ID);
            youTubePlayer.loadVideo(VIDEO_ID);
            //youTubePlayer.setFullscreen(true);
            youTubePlayer.play();
        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Erreur d'initialisation de la vidéo!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /*===============================================================*/

    //La méthode qui retourne la liste des vidéos
    private List<Rubriques> get_Rubriques(){
        List<Rubriques> rubriques = new ArrayList<>();
        Rubriques rubrique;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/rubriques/list_rubrique.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    rubrique = new Rubriques();
                    rubrique.setId(jsonObject.optInt("id"));
                    rubrique.setImage(jsonObject.optString("image"));
                    rubrique.setNom(jsonObject.optString("nom"));
                    rubriques.add(rubrique);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rubriques;
    }
}
