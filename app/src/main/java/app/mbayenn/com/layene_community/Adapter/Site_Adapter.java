package app.mbayenn.com.layene_community.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Sites;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 29/01/2017.
 */

public class Site_Adapter extends BaseAdapter {
    private Context context;
    private List<Sites> sites;
    private LayoutInflater inflater;

    public Site_Adapter(Context context, List<Sites> sites) {
        this.context = context;
        this.sites = sites;
    }

    @Override
    public int getCount() {
        return sites.size();
    }

    @Override
    public Object getItem(int i) {
        return sites.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.site_item,null);
        ImageView img_site= (ImageView)view.findViewById(R.id.img_site);
        TextView txt_titre = (TextView)view.findViewById(R.id.txt_titre_site);
        TextView txt_description = (TextView)view.findViewById(R.id.txt_description_site);
        LinearLayout l_share_site = (LinearLayout)view.findViewById(R.id.l_share_site);
        final Sites s = sites.get(i);
        String link_img ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/sites/"+s.getImage();
        Glide.with(context).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_site);
        txt_titre.setText(s.getTitre());
        txt_description.setText(s.getDescription());
        l_share_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, s.getTitre());
                share.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>"+s.getDescription()+"</p>"));
                context.startActivity(Intent.createChooser(share, "Partager ce site!"));
            }
        });
        return view;
    }
}
