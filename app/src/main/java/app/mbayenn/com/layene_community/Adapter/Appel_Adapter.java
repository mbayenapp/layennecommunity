package app.mbayenn.com.layene_community.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.Activity.Start_video_categorie;
import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 10/12/2016.
 */

public class Appel_Adapter extends RecyclerView.Adapter<Appel_Adapter.MyViewHolder> {
    private Activity context;
    private List<Videos> videosItems;

    public Appel_Adapter(Activity context, List<Videos> videosItems) {
        this.context = context;
        this.videosItems = videosItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.v_categorie_item, parent, false);

        return new Appel_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Videos video = videosItems.get(position);
        holder.txt_titre_video.setText(video.getTitre());
        String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/videos/"+video.getImage();
        Glide.with(context).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_video);
        holder.card_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(context,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Initialisation de la vidéo...");
                progressDialog.show();
                new CountDownTimer(2000,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        progressDialog.dismiss();
                        Intent intent = new Intent(context.getApplicationContext(),Start_video_categorie.class);
                        intent.putExtra("id_video",video.getId_video());
                        intent.putExtra("categorie",video.getCategorie());
                        intent.putExtra("titre",video.getTitre());
                        context.startActivity(intent);
                        context.finish();
                        context.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                }.start();
            }
        });
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(context,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Initialisation de la vidéo...");
                progressDialog.show();
                new CountDownTimer(2000,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        progressDialog.dismiss();
                        Intent intent = new Intent(context.getApplicationContext(),Start_video_categorie.class);
                        intent.putExtra("id_video",video.getId_video());
                        intent.putExtra("categorie",video.getCategorie());
                        intent.putExtra("titre",video.getTitre());
                        context.startActivity(intent);
                        context.finish();
                        context.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                }.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return videosItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_video;
        public TextView txt_titre_video;
        public CardView card_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_video = (ImageView)itemView.findViewById(R.id.img_tv_rubrique);
            txt_titre_video = (TextView)itemView.findViewById(R.id.txt_nom_tv_runbrique);
            card_video = (CardView)itemView.findViewById(R.id.card_tv);
        }
    }
}
