package app.mbayenn.com.layene_community.Activity;

import android.content.DialogInterface;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

public class Contact extends AppCompatActivity {
    private AwesomeValidation awesomeValidation,awe;
    private Toolbar toolbar;
    private EditText txt_nom,txt_email,txt_message;
    private Button btn_send_message;
    private ConnexionInternet connexionInternet;
    private String token = "layenne2017";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        /*========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_contact);
        toolbar.setTitle("Nous contacter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        btn_send_message = (Button)findViewById(R.id.btn_send_msg);
        txt_nom = (EditText)findViewById(R.id.nom_contact);
        txt_email = (EditText)findViewById(R.id.email_contact);
        txt_message = (EditText)findViewById(R.id.text_contact);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awe = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(Contact.this,R.id.nom_contact,"[a-zA-Z\\s]+",R.string.err_empty_nom);
        awesomeValidation.addValidation(Contact.this,R.id.email_contact,"[a-zA-Z0-9\\s]+",R.string.err_empty_email);
        awesomeValidation.addValidation(Contact.this,R.id.text_contact,"[a-zA-Z0-9\\s]+",R.string.err_empty_message);
        awe.addValidation(Contact.this,R.id.email_contact, Patterns.EMAIL_ADDRESS,R.string.err_email);
        /*========================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(Contact.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Contact.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            btn_send_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String nom = txt_nom.getText().toString().trim();
                    String email = txt_email.getText().toString().trim();
                    String message = txt_message.getText().toString().trim();
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                    if (nom.equals("")) {
                        awesomeValidation.validate();
                    } else {
                        awesomeValidation.clear();
                    }
                    if (email.equals("")) {
                        awesomeValidation.validate();
                    } else {
                        awesomeValidation.clear();
                    }
                    if (email.equals("")) {
                        awesomeValidation.validate();
                    } else {
                        awesomeValidation.clear();
                    }
                    if (!nom.equals("") && !email.equals("") && !message.equals("")) {
                        if (email.matches(emailPattern)) {
                            String reponse = send_msg(nom, email, message);
                            if (reponse.equals("ok")) {
                                Toast.makeText(getApplicationContext(), "Votre message a été envoyé avec succès!", Toast.LENGTH_LONG).show();
                                txt_nom.setText("");
                                txt_email.setText("");
                                txt_message.setText("");
                            } else {
                                Toast.makeText(getApplicationContext(), reponse, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            awe.validate();
                        }
                    }
                }
            });
        }
    }
    /*============================================================*/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
        }
        return false;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
        }
        return super.onOptionsItemSelected(item);
    }
    /*===============================================================*/
    //La méthode qui envoi un message de contact
    private String send_msg(String nom,String email,String message){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        //String link = "http://marrakech4holidays.com/Carte_Visite_API/Service_Api/contact.php?email="+email+"&nom="+nom+"&message="+URLEncoder.encode(message);
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/contact.php?nom="+URLEncoder.encode(nom)+"&email="+email+"&message="+URLEncoder.encode(message)+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
