package app.mbayenn.com.layene_community.Utils;

/**
 * Created by bfali on 13/11/2016.
 */

public class Verification_Date {
    public Verification_Date() {
    }

    public String get_date(String date){
        String resul = "";
        //La date du système
        String format = "dd/MM/yyyy H:mm:ss";
        java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
        java.util.Date d = new java.util.Date();
        String dact = String.valueOf(formater.format(d));
        String[] p = dact.split(" ");
        String[] Fdact = p[0].split("/");
        String[] Fhact = p[1].split(":");
        String jourAct = Fdact[0];
        String moiAct = Fdact[1];
        String anneeAct = Fdact[2];
        String heureAct = Fhact[0];
        String minAct = Fhact[1];
        //La date de mon article
        String[] part = date.split(" ");
        String[] Fd = part[0].split("-");
        String Horaire = part[1];
        String[] Fh = Horaire.split(":");
        String anneeArticle = Fd[0];
        String moiArticle = Fd[1];
        String jourArticle = Fd[2];
        String heureArticle = Fh[0];
        String minArticle = Fh[1];
        if (heureArticle.equals(heureAct)){
            resul = heureArticle+":"+minArticle;
        }else if (!moiArticle.equals(moiAct) || !jourArticle.equals(jourAct)){
            resul ="Le "+jourArticle+", "+get_date(Integer.parseInt(moiArticle));
        }else if (!anneeArticle.equals(anneeAct)){
            resul ="Le "+jourArticle+", "+get_date(Integer.parseInt(moiArticle))+" "+anneeArticle;
        }else {
            resul = heureArticle+":"+minArticle;
        }
        return resul;
    }

    //La méthode qui convertie la date
    public String get_date(int ch){
        String lettre = "";
        int o = 0;
        int h = 8;
        int n = 9;
        if (ch == 01){
            lettre = "Janv";
        }else if (ch == 02){
            lettre = "Fev";
        }else if (ch == 03){
            lettre = "Mars";
        }else if (ch == 04){
            lettre = "Avril";
        }else if (ch == 05){
            lettre = "Mai";
        }else if (ch == 06){
            lettre = "Juin";
        }else if (ch == 07){
            lettre = "Juil";
        }else if (ch == o+h){
            lettre = "Août";
        }else if (ch == o+n){
            lettre = "Sept";
        }else if (ch == 10){
            lettre = "Oct";
        }else if (ch == 11){
            lettre = "Nov";
        }else{
            lettre = "Dec";
        }
        return lettre;
    }
}
