package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.net.URL;

import app.mbayenn.com.layene_community.Helper.SessionManagerUser;
import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;

public class Login extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private SessionManagerUser sessionManagerUser;
    private Button btn_login,btn_register;
    private TextView txt_forgot;
    private EditText txt_email,txt_password;
    private ProgressDialog progressDialog;
    private AwesomeValidation awesomeValidation;
    private Users u;
    private LinearLayout l_content_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*======================================================================*/
        sessionManagerUser = new SessionManagerUser(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_login);
        toolbar.setTitle("Connexion");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //Vérifier si une session est ouvert
        if (!sessionManagerUser.getLoggedInUser().getEmail().equals("")) {
            u = get_user(sessionManagerUser.getLoggedInUser().getEmail());
            final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Patienter un instant...");
            progressDialog.show();
            new CountDownTimer(3000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    Intent intent = new Intent(getApplicationContext(), ProfileUser.class);
                    intent.putExtra("email", u.getEmail());
                    intent.putExtra("profil", u.getProfil());
                    intent.putExtra("nom", u.getPrenom() + " " + u.getNom());
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    progressDialog.dismiss();

                }
            }.start();
        }
        /*==========================================================*/
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_register = (Button)findViewById(R.id.btn_register);
        txt_forgot = (TextView)findViewById(R.id.txt_forgot_password);
        txt_email = (EditText)findViewById(R.id.email);
        txt_password = (EditText)findViewById(R.id.password);
        l_content_login = (LinearLayout)findViewById(R.id.l_content_login);
        /*==========================================================*/
        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Reset_password.class));
                overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                finish();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Register.class));
                overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                finish();
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txt_email.getText().toString().trim();
                String password = txt_password.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
                awesomeValidation.addValidation(Login.this,R.id.email, Patterns.EMAIL_ADDRESS,R.string.err_email);
                if (!email.equals("") || !password.equals("")){
                    if(email.matches(emailPattern)) {
                        if (password.length() < 6){
                            String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}";
                            awesomeValidation.addValidation(Login.this, R.id.password, regexPassword, R.string.err_password);
                            awesomeValidation.validate();
                        }else {
                            awesomeValidation.clear();
                            final Users user = get_user(email,password);
                            if (user.getId() != 0){
                                final ProgressDialog progressDialog = new ProgressDialog(Login.this,
                                        R.style.AppTheme_Dark_Dialog);
                                progressDialog.setIndeterminate(true);
                                progressDialog.setMessage("Connexion en cours...");
                                progressDialog.show();
                                Users us = new Users(user.getEmail());
                                sessionManagerUser.storeUser(us);
                                new CountDownTimer(3000,1000){

                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        Intent intent = new Intent(getApplicationContext(),ProfileUser.class);
                                        intent.putExtra("email",user.getEmail());
                                        intent.putExtra("profil", user.getProfil());
                                        intent.putExtra("nom",user.getPrenom()+" "+user.getNom());
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                        progressDialog.dismiss();
                                    }
                                }.start();
                            }else {
                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Login.this);
                                alertDialogBuilder.setTitle("Informations incorrectes");
                                alertDialogBuilder.setMessage("Il se peut que l'adresse email ou le mot de passe que vous avez saisi soit incorrect(e). Veuillez ré-essayer!");

                                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.hide();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        }
                    }else {
                        awesomeValidation.validate();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Tous les champs doivent être rempli!",Toast.LENGTH_LONG).show();
                }
            }
        });
        /*==========================================================*/
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
        return false;
    }

    /*=========================================================================*/
    //La fonction qui récupére les informations de l'utilisateurs dont l'email est fournie en paremètre
    private Users get_user(String email){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/get_user.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setIndicatif(jsonObject.optString("indicatif"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
    private Users get_user(String email,String password){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/login.php?email="+email+"&password="+password+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setIndicatif(jsonObject.optString("indicatif"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
    //La méthode qui vérifie si le compte existe
    private int is_allow(String email){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        int nbr = 0;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/is_allow.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            nbr = Integer.parseInt(result);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return nbr;
    }
}
