package app.mbayenn.com.layene_community.Fragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Model.Issas;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Issa.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Issa#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Issa extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String token = "layenne2017";
    private OnFragmentInteractionListener mListener;
    private ImageView img_issa,img_tempe;
    private Issas issa = get_Issa();
    private TextView titre,content;
    private SwipeRefreshLayout swipe_issa;
    private ConnexionInternet connexionInternet;

    public Issa() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Issa.
     */
    // TODO: Rename and change types and number of parameters
    public static Issa newInstance(String param1, String param2) {
        Issa fragment = new Issa();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_issa, container, false);
        img_issa = (ImageView)view.findViewById(R.id.img_issa);
        img_tempe = (ImageView)view.findViewById(R.id.img_tempe);
        titre = (TextView)view.findViewById(R.id.txt_titre_issa);
        content = (TextView)view.findViewById(R.id.txt_content_issa);

        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            swipe_issa = (SwipeRefreshLayout) view.findViewById(R.id.swipe_issa);
            swipe_issa.setOnRefreshListener(Issa.this);
            swipe_issa.setColorSchemeColors(Color.parseColor("#01315a"));
            swipe_issa.setRefreshing(true);
            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/Issa/" + issa.getImage();
                    Glide.with(Issa.this).load(link_img).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_issa);
                    String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/Issa/" + issa.getLogo();
                    Glide.with(Issa.this).load(link).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_tempe);
                    titre.setText(issa.getTitre());
                    content.setText(issa.getContent());
                    swipe_issa.setRefreshing(false);
                }
            }.start();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_issa.postDelayed(new Runnable() {
            @Override
            public void run() {
                String link_img ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/Issa/"+issa.getImage();
                Glide.with(Issa.this).load(link_img).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_issa);
                String link ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/Issa/"+issa.getLogo();
                Glide.with(Issa.this).load(link).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_tempe);
                titre.setText(issa.getTitre());
                content.setText(issa.getContent());
                swipe_issa.setRefreshing(false);
            }
        },2000);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //La methode qui information de Seydina Issa
    private Issas get_Issa(){
        Issas issa = new Issas();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Issa/info.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                issa.setId(jsonObject.optInt("id"));
                issa.setTitre(jsonObject.optString("titre"));
                issa.setContent(jsonObject.optString("content"));
                issa.setImage(jsonObject.optString("image"));
                issa.setLogo(jsonObject.optString("logo"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return issa;
    }
}
