package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;

public class Complete_profil extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private String email,nom,prenom;
    private LinearLayout layout_message_bienvenue,layout_first_form,
            l_bienvenu_title,l_bienvenu_pic,l_bienvenu_progress,
            l_bienvenue_message,l_bienvenu_name,l_bienvenu_btn;
    private Button btn_complete,btn_suivant;
    private EditText txt_ville,txt_code_postal,txt_pays,txt_tel;
    private RadioGroup group;
    private RadioButton RadioSelBtu;
    private AwesomeValidation awesomeValidation;
    private Users userM;
    private TextView txt_name;
    private Spinner sp_indication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profil);
        /*=======================================================*/
        final Bundle extras = getIntent().getExtras();
        email = extras.getString("email");
        nom = extras.getString("nom");
        prenom = extras.getString("prenom");
        toolbar = (Toolbar)findViewById(R.id.toolbar_complete_profil);
        toolbar.setTitle("Compléter votre profil");
        setSupportActionBar(toolbar);
        /*=======================================================*/
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(Complete_profil.this,R.id.ville,"[a-zA-Z\\s]+",R.string.err_ville);
        awesomeValidation.addValidation(Complete_profil.this,R.id.pays,"[a-zA-Z\\séèç]+",R.string.err_pays);
        awesomeValidation.addValidation(Complete_profil.this,R.id.tel, RegexTemplate.TELEPHONE,R.string.err_phone);
        layout_message_bienvenue = (LinearLayout)findViewById(R.id.layout_message_bienvenue);
        layout_first_form = (LinearLayout)findViewById(R.id.layout_first_form);
        l_bienvenu_title = (LinearLayout)findViewById(R.id.l_bienvenu_title);
        l_bienvenu_pic = (LinearLayout)findViewById(R.id.l_bienvenu_pic);
        l_bienvenu_progress = (LinearLayout)findViewById(R.id.l_bienvenu_progress);
        l_bienvenue_message =(LinearLayout)findViewById(R.id.l_bienvenue_message);
        l_bienvenu_name = (LinearLayout)findViewById(R.id.l_bienvenu_name);
        l_bienvenu_btn = (LinearLayout)findViewById(R.id.l_bienvenu_btn);
        btn_complete = (Button)findViewById(R.id.btn_complete);
        btn_suivant = (Button)findViewById(R.id.btn_save_first);
        group = (RadioGroup) findViewById(R.id.sexeChoisi);
        txt_ville = (EditText)findViewById(R.id.ville);
        txt_code_postal = (EditText)findViewById(R.id.code_postal);
        txt_pays = (EditText)findViewById(R.id.pays);
        txt_tel = (EditText)findViewById(R.id.tel);
        txt_name = (TextView)findViewById(R.id.txt_bienvenu_name);
        sp_indication = (Spinner)findViewById(R.id.txt_indicatif);
        /*=======================================================*/
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                l_bienvenu_progress.setVisibility(View.GONE);
                l_bienvenu_title.setVisibility(View.VISIBLE);
                l_bienvenu_pic.setVisibility(View.VISIBLE);
                l_bienvenue_message.setVisibility(View.VISIBLE);
                l_bienvenu_name.setVisibility(View.VISIBLE);
                l_bienvenu_btn.setVisibility(View.VISIBLE);
                txt_name.setText(prenom+" "+nom);
            }
        }.start();
        btn_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(Complete_profil.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Un instant...");
                progressDialog.show();
                new CountDownTimer(2000,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        layout_message_bienvenue.setVisibility(View.GONE);
                        layout_first_form.setVisibility(View.VISIBLE);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        progressDialog.dismiss();
                    }
                }.start();
            }
        });
        btn_suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ville = txt_ville.getText().toString().trim();
                String code_postal = txt_code_postal.getText().toString().trim();
                String pays = txt_pays.getText().toString().trim();
                String tel = txt_tel.getText().toString().trim();
                int typeSeel = group.getCheckedRadioButtonId();
                RadioSelBtu = (RadioButton) findViewById(typeSeel);
                String indicatif = String.valueOf(sp_indication.getSelectedItem());
                final String sexe =  RadioSelBtu.getText().toString();
                if (!ville.equals("")|| !pays.equals("") || !tel.equals("")){
                    awesomeValidation.validate();
                    userM = new Users(sexe,indicatif,tel,ville,Integer.parseInt(code_postal),pays,email);
                    String reponse = manageUser(userM);
                    if (reponse.equals("ok")){
                        final ProgressDialog progressDialog = new ProgressDialog(Complete_profil.this,
                                R.style.AppTheme_Dark_Dialog);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Enregistrement en cours...");
                        progressDialog.show();
                        new CountDownTimer(3000,1000){

                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                Intent intent = new Intent(getApplicationContext(),Manage_Profil.class);
                                intent.putExtra("email",email);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                progressDialog.dismiss();
                                finish();
                            }
                        }.start();
                    }else {
                        Toast.makeText(getApplicationContext(),reponse,Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Vous devez indiquer la ville, le code postal et votre téléphone",Toast.LENGTH_LONG).show();
                }

            }
        });
    }
    //La méthode qui modifie un utilisateur
    private String manageUser(Users user){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/manage_compte.php?ville="+user.getVille()+"&code_postal="+user.getCode_postal()+"&pays="+user.getPays()+"&indicatif="+user.getIndicatif()+"&tel="+user.getTel()+"&sexe="+user.getSexe()+"&email="+user.getEmail()+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
