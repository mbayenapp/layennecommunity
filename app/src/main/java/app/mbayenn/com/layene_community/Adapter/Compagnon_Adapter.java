package app.mbayenn.com.layene_community.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Compagnons;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 16/03/2017.
 */

public class Compagnon_Adapter extends BaseAdapter {
    private Context context;
    private List<Compagnons> compagnons;
    private LayoutInflater inflater;

    public Compagnon_Adapter(Context context, List<Compagnons> compagnons) {
        this.context = context;
        this.compagnons = compagnons;
    }

    @Override
    public int getCount() {
        return compagnons.size();
    }

    @Override
    public Object getItem(int i) {
        return compagnons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.compagnon_item,null);
        ExpandableTextView expTv1 = (ExpandableTextView)view.findViewById(R.id.expand_text_view)
                .findViewById(R.id.expand_text_view);
        TextView txt_titre = (TextView)view.findViewById(R.id.txt_titre_compagon);
        final Compagnons compagnon = compagnons.get(i);
        txt_titre.setText(compagnon.getNom());
        expTv1.setText(Html.fromHtml("<p>"+compagnon.getDescription()+"</p>"));
        return view;
    }
}
