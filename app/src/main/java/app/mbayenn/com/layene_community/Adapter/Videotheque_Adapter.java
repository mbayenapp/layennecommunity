package app.mbayenn.com.layene_community.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.Activity.Start_Video;
import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 15/11/2016.
 */

public class Videotheque_Adapter extends RecyclerView.Adapter<Videotheque_Adapter.MyViewHolder> {
    private Context context;
    private List<Videos> videosItems;

    public Videotheque_Adapter(Context context, List<Videos> videosItems) {
        this.context = context;
        this.videosItems = videosItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Videos video = videosItems.get(position);
        holder.txt_titre_video.setText(video.getTitre());
        holder.txt_categorie.setText(video.getCategorie());
        String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/videos/"+video.getImage();
        Glide.with(context).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_video);
        holder.card_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Start_Video.class);
                intent.putExtra("id",video.getId());
                intent.putExtra("titre",video.getTitre());
                intent.putExtra("description",video.getDescription());
                intent.putExtra("categorie",video.getCategorie());
                intent.putExtra("id_video",video.getId_video());
                context.startActivity(intent);
            }
        });
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Start_Video.class);
                intent.putExtra("id",video.getId());
                intent.putExtra("titre",video.getTitre());
                intent.putExtra("description",video.getDescription());
                intent.putExtra("categorie",video.getCategorie());
                intent.putExtra("id_video",video.getId_video());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return videosItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_video;
        public TextView txt_titre_video,txt_categorie;
        public CardView card_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_video = (ImageView)itemView.findViewById(R.id.image_video);
            txt_titre_video = (TextView)itemView.findViewById(R.id.title_video);
            txt_categorie = (TextView)itemView.findViewById(R.id.categorie);
            card_video = (CardView)itemView.findViewById(R.id.mcard_video);
        }

    }
}
