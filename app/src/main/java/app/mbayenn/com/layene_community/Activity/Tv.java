package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Model.Directes;
import app.mbayenn.com.layene_community.Model.Rubriques;
import app.mbayenn.com.layene_community.Adapter.Rubrique_Adapter;
import app.mbayenn.com.layene_community.Model.Articles;
import app.mbayenn.com.layene_community.Model.Directes;
import app.mbayenn.com.layene_community.Model.Rubriques;
import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

public class Tv extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private ImageView imgdiamtv;
    private RecyclerView r_rubrique;
    private Rubrique_Adapter adapter;
    private ConnexionInternet connexionInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv);
        /*========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_tv);
        toolbar.setTitle("TV");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        imgdiamtv = (ImageView)findViewById(R.id.diamtv);
        r_rubrique = (RecyclerView)findViewById(R.id.rubrique);
        /*========================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(Tv.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Tv.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/source/diamtv.png";
            Glide.with(Tv.this).load(link).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(imgdiamtv);
            List<Rubriques> rubriques = get_Rubriques();
            adapter = new Rubrique_Adapter(Tv.this, rubriques);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
            r_rubrique.setLayoutManager(mLayoutManager);
            r_rubrique.addItemDecoration(new Tv.GridSpacingItemDecoration(2, dpToPx(10), true));
            r_rubrique.setItemAnimator(new DefaultItemAnimator());
            r_rubrique.setAdapter(adapter);
            imgdiamtv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ProgressDialog progressDialog = new ProgressDialog(Tv.this,
                            R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Initialisation de la vidéo...");
                    progressDialog.show();
                    final Directes directe = get_Last_Directe();
                    new CountDownTimer(3000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            progressDialog.dismiss();
                            Intent intent = new Intent(getApplicationContext(), Start_Directe.class);
                            intent.putExtra("id_video", directe.getId_video());
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                        }
                    }.start();
                }
            });
        }
    }
    /*===============================================================*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
        }
        return false;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /*===============================================================*/

    //La méthode qui retourne la liste des vidéos
    private List<Rubriques> get_Rubriques(){
        List<Rubriques> rubriques = new ArrayList<>();
        Rubriques rubrique;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/rubriques/list_rubrique.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    rubrique = new Rubriques();
                    rubrique.setId(jsonObject.optInt("id"));
                    rubrique.setImage(jsonObject.optString("image"));
                    rubrique.setNom(jsonObject.optString("nom"));
                    rubriques.add(rubrique);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rubriques;
    }
    /*=======================================================================*/
    //La méthode qui retourne le dernier article
    private Directes get_Last_Directe(){
        Directes directe = new Directes();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Tv/last_directe.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                directe.setId(jsonObject.optInt("id"));
                directe.setId_video(jsonObject.optString("id_video"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directe;
    }
}
