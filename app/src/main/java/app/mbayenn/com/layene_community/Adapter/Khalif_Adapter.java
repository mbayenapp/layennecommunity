package app.mbayenn.com.layene_community.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Khalifs;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 29/01/2017.
 */

public class Khalif_Adapter extends BaseAdapter {
    private Context context;
    private List<Khalifs> khalifs;
    private LayoutInflater inflater;

    public Khalif_Adapter(Context context, List<Khalifs> khalifs) {
        this.context = context;
        this.khalifs = khalifs;
    }

    @Override
    public int getCount() {
        return khalifs.size();
    }

    @Override
    public Object getItem(int i) {
        return khalifs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.khalif_item,null);
        ImageView img_khalif = (ImageView)view.findViewById(R.id.img_khalif);
        TextView txt_nom = (TextView)view.findViewById(R.id.txt_titre_khalif);
        TextView txt_description = (TextView)view.findViewById(R.id.txt_description_khalif);
        TextView txt_periode = (TextView)view.findViewById(R.id.txt_int_khalifat);
        final Khalifs kh = khalifs.get(i);
        String link_img ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/khalifs/"+kh.getPhoto();
        Glide.with(context).load(link_img).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_khalif);
        txt_nom.setText(kh.getNom());
        txt_description.setText(kh.getDescription());
        if (kh.getDate_out().equals("null")){
            txt_periode.setText("Khalif actuel");
        }else {
            txt_periode.setText(kh.getDate_in()+" - "+kh.getDate_out());
        }
        return view;
    }
}
