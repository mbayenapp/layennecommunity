package app.mbayenn.com.layene_community.Utils;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Adapter.Mahdi_Adapter;
import app.mbayenn.com.layene_community.Model.Mahdi;
import app.mbayenn.com.layene_community.Adapter.Mahdi_Adapter;
import app.mbayenn.com.layene_community.Model.Mahdi;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 27/01/2017.
 */

public class tabMahdi extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private String token = "layenne2017";
    private SwipeRefreshLayout swipe_mahdi;
    private ListView list_mahdis;
    private List<Mahdi> mahdis;
    private Mahdi_Adapter adapter;
    private ConnexionInternet connexionInternet;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabmahdi, container, false);
        swipe_mahdi = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_mahdi);
        list_mahdis = (ListView)rootView.findViewById(R.id.list_mahdi);

        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            swipe_mahdi.setOnRefreshListener(tabMahdi.this);
            swipe_mahdi.setColorSchemeColors(Color.parseColor("#01315a"));
            swipe_mahdi.setRefreshing(true);
            mahdis = getMahdis();
        }
         /*===================================================================*/
        new CountDownTimer(2000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                adapter = new Mahdi_Adapter(getActivity(),mahdis);
                list_mahdis.setAdapter(adapter);
                swipe_mahdi.setRefreshing(false);
            }
        }.start();
        return rootView;
    }

    @Override
    public void onRefresh() {
        swipe_mahdi.postDelayed(new Runnable() {
            @Override
            public void run() {
                mahdis.clear();
                mahdis.addAll(getMahdis());
                adapter.notifyDataSetChanged();
                swipe_mahdi.setRefreshing(false);
            }
        },2000);
    }
    /*===================================================================*/
    //La méthode qui retourne une liste sur le mahdi
    private List<Mahdi> getMahdis(){
        List<Mahdi> mahdis = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        Mahdi mahdi;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Mahdi/list_mahdi.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            JSONArray jsonArray = new JSONArray(result);
            for (int i = 0; i < jsonArray.length();i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                mahdi = new Mahdi();
                mahdi.setId(jsonObject.optInt("id"));
                mahdi.setTitre(jsonObject.optString("titre"));
                mahdi.setContent(jsonObject.optString("content"));
                mahdis.add(mahdi);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mahdis;
    }
}
