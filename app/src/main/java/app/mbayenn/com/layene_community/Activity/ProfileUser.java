package app.mbayenn.com.layene_community.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.EachExceptionsHandler;
import com.kosalgeek.genasync12.PostResponseAsyncTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import app.mbayenn.com.layene_community.Helper.SessionManagerUser;
import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileUser extends AppCompatActivity {
    private String token = "layenne2017";
    private Uri mCropImageUri;
    private static final int PICK_IMAGE_REQUEST = 234;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private Toolbar toolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private CircleImageView img_profil_user;
    private TextView txt_name_user,txt_adresse_user_connect,txt_ville_user_connect,txt_pays_user_connect,txt_tel_user_connect;
    private TextView txt_add_ville_user_connect,txt_add_pays_user_connect,txt_add_tel_user_connect;
    private String link_img;
    private SessionManagerUser sessionManagerUser;
    private LayoutInflater inflater;
    private Users u;
    private ImageView img_manage_couverture,iv_couverture_profil_user,im_act_profil;
    private ConnexionInternet connexionInternet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);
        /*============================================================*/
        Bundle extras = getIntent().getExtras();
        final String email = extras.getString("email");
        String profil = extras.getString("profil");
        final String name = extras.getString("nom");
        toolbar = (Toolbar)findViewById(R.id.toolbar_profil_user);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*============================================================*/
        sessionManagerUser = new SessionManagerUser(ProfileUser.this);
        img_profil_user = (CircleImageView)findViewById(R.id.img_profil_user_connect);
        txt_name_user = (TextView)findViewById(R.id.txt_name_user_connect);
        txt_adresse_user_connect = (TextView)findViewById(R.id.txt_adresse_user_connect);
        txt_ville_user_connect = (TextView)findViewById(R.id.txt_ville_user_connect);
        txt_pays_user_connect = (TextView)findViewById(R.id.txt_pays_user_connect);
        txt_tel_user_connect = (TextView)findViewById(R.id.txt_tel_user_connect);
        txt_add_ville_user_connect = (TextView)findViewById(R.id.txt_add_ville_user_connect);
        txt_add_pays_user_connect = (TextView)findViewById(R.id.txt_add_pays_user_connect);
        txt_add_tel_user_connect = (TextView)findViewById(R.id.txt_add_tel_user_connect);
        img_manage_couverture = (ImageView)findViewById(R.id.img_manage_couverture);
        iv_couverture_profil_user = (ImageView)findViewById(R.id.iv_couverture_profil_user);
        im_act_profil = (ImageView)findViewById(R.id.im_act_profil);
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(ProfileUser.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(ProfileUser.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            u = get_user(email);
        /*============================================================*/
            if (!sessionManagerUser.getLoggedInUser().getEmail().equals("")) {
                u = get_user(sessionManagerUser.getLoggedInUser().getEmail());
                txt_adresse_user_connect.setText(u.getAdresse());
                txt_ville_user_connect.setText(u.getVille());
                txt_pays_user_connect.setText(u.getPays());
                txt_tel_user_connect.setText(u.getIndicatif() + " " + u.getTel());
            }
                if (!u.getProfil().equals("null")) {
                    link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/" + profil;
                } else {
                    link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/boy.png";
                }
            if (!u.getCouverture().equals("null")) {
                String link_couverture = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/couverture/" + u.getCouverture();
                Glide.with(ProfileUser.this).load(link_couverture).centerCrop().crossFade().placeholder(R.drawable.logo).into(iv_couverture_profil_user);
                //link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/"+u.getProfil();
            }
            Glide.with(this).load(link_img)
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.boy)
                    .fitCenter()
                    .into(img_profil_user);
            txt_name_user.setText(name);
            if (u.getVille().equals("")) {
                txt_ville_user_connect.setVisibility(View.GONE);
                txt_add_ville_user_connect.setVisibility(View.VISIBLE);
            } else {
                txt_ville_user_connect.setVisibility(View.VISIBLE);
                txt_add_ville_user_connect.setVisibility(View.GONE);
            }
            if (u.getPays().equals("")) {
                txt_pays_user_connect.setVisibility(View.GONE);
                txt_add_pays_user_connect.setVisibility(View.VISIBLE);
            } else {
                txt_pays_user_connect.setVisibility(View.VISIBLE);
                txt_add_pays_user_connect.setVisibility(View.GONE);
            }
            if (u.getTel().equals("")) {
                txt_tel_user_connect.setVisibility(View.GONE);
                txt_add_tel_user_connect.setVisibility(View.VISIBLE);
            } else {
                txt_tel_user_connect.setVisibility(View.VISIBLE);
                txt_add_tel_user_connect.setVisibility(View.GONE);
            }
            im_act_profil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inflater = ProfileUser.this.getLayoutInflater();
                    View content = inflater.inflate(R.layout.action_item, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileUser.this);
                    final TextView txt_manage_profil = (TextView) content.findViewById(R.id.txt_manage_profil);
                    builder.setView(content);
                    txt_manage_profil.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            builder.create().dismiss();
                            startActivity(new Intent(getApplicationContext(), Manage_pic_profil.class));
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            finish();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                }
            });
            txt_add_ville_user_connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    inflater = ProfileUser.this.getLayoutInflater();
                    View content = inflater.inflate(R.layout.manage_item, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileUser.this);
                    final LinearLayout l_add_ville = (LinearLayout) content.findViewById(R.id.l_manage_ville);
                    final EditText txt_ville = (EditText) content.findViewById(R.id.villeA);
                    l_add_ville.setVisibility(View.VISIBLE);
                    builder.setView(content).setTitle("Ajouter la ville actuelle")
                            .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String ville = txt_ville.getText().toString().trim();
                                    if (ville.equals("")) {
                                        Toast.makeText(getApplicationContext(), "Vueillez indiquer la ville", Toast.LENGTH_LONG).show();
                                    } else {
                                        manage_ville(ville, email);
                                        final ProgressDialog progressDialog = new ProgressDialog(ProfileUser.this,
                                                R.style.AppTheme_Dark_Dialog);
                                        progressDialog.setIndeterminate(true);
                                        progressDialog.setMessage("Un instant...");
                                        progressDialog.show();
                                        new CountDownTimer(1500, 1000) {

                                            @Override
                                            public void onTick(long millisUntilFinished) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                l_add_ville.setVisibility(View.GONE);
                                                Intent intent = new Intent(getApplicationContext(), ProfileUser.class);
                                                intent.putExtra("email", email);
                                                intent.putExtra("profil", u.getProfil());
                                                intent.putExtra("nom", name);
                                                startActivity(intent);
                                            }
                                        }.start();
                                    }
                                }
                            }).setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            txt_add_pays_user_connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inflater = ProfileUser.this.getLayoutInflater();
                    View content = inflater.inflate(R.layout.manage_item, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileUser.this);
                    final LinearLayout l_add_pays = (LinearLayout) content.findViewById(R.id.l_manage_pays);
                    final EditText txt_pays = (EditText) content.findViewById(R.id.paysA);
                    l_add_pays.setVisibility(View.VISIBLE);
                    builder.setView(content).setTitle("Ajouter votre pays actuel")
                            .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String pays = txt_pays.getText().toString().trim();
                                    if (pays.equals("")) {
                                        Toast.makeText(getApplicationContext(), "Vueillez indiquer la ville", Toast.LENGTH_LONG).show();
                                    } else {
                                        manage_pays(pays, email);
                                        final ProgressDialog progressDialog = new ProgressDialog(ProfileUser.this,
                                                R.style.AppTheme_Dark_Dialog);
                                        progressDialog.setIndeterminate(true);
                                        progressDialog.setMessage("Un instant...");
                                        progressDialog.show();
                                        new CountDownTimer(1500, 1000) {

                                            @Override
                                            public void onTick(long millisUntilFinished) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                l_add_pays.setVisibility(View.GONE);
                                                Intent intent = new Intent(getApplicationContext(), ProfileUser.class);
                                                intent.putExtra("email", email);
                                                intent.putExtra("profil",u.getProfil());
                                                intent.putExtra("nom", name);
                                                startActivity(intent);
                                            }
                                        }.start();
                                    }
                                }
                            }).setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            txt_add_tel_user_connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inflater = ProfileUser.this.getLayoutInflater();
                    View content = inflater.inflate(R.layout.manage_item, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileUser.this);
                    final LinearLayout l_add_pays = (LinearLayout) content.findViewById(R.id.l_manage_tel);
                    final EditText txt_pays = (EditText) content.findViewById(R.id.telA);
                    final Spinner sp_indicatif = (Spinner) content.findViewById(R.id.txt_indicatifA);
                    l_add_pays.setVisibility(View.VISIBLE);
                    builder.setView(content).setTitle("Ajouter votre numéro de téléhone")
                            .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final String tel = txt_pays.getText().toString().trim();
                                    final String indicatif = String.valueOf(sp_indicatif.getSelectedItem());
                                    if (tel.equals("")) {
                                        Toast.makeText(getApplicationContext(), "Vueillez indiquer la ville", Toast.LENGTH_LONG).show();
                                    } else {
                                        manage_tel(indicatif, tel, email);
                                        final ProgressDialog progressDialog = new ProgressDialog(ProfileUser.this,
                                                R.style.AppTheme_Dark_Dialog);
                                        progressDialog.setIndeterminate(true);
                                        progressDialog.setMessage("Un instant...");
                                        progressDialog.show();
                                        new CountDownTimer(1500, 1000) {

                                            @Override
                                            public void onTick(long millisUntilFinished) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                u = get_user(email);
                                                txt_tel_user_connect.setText(indicatif + " " + tel);
                                                l_add_pays.setVisibility(View.GONE);
                                                Intent intent = new Intent(getApplicationContext(), ProfileUser.class);
                                                intent.putExtra("email", email);
                                                intent.putExtra("profil", u.getProfil());
                                                intent.putExtra("nom", name);
                                                startActivity(intent);
                                            }
                                        }.start();
                                    }
                                }
                            }).setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            if (sessionManagerUser.getLoggedInUser().getId() != 0) {
                txt_adresse_user_connect.setText(u.getAdresse());
                txt_ville_user_connect.setText(u.getVille());
                txt_pays_user_connect.setText(u.getPays());
                txt_tel_user_connect.setText(u.getIndicatif() + " " +u.getTel());
            }
            img_manage_couverture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectImageClick(v);
                }
            });
        }
        /*============================================================*/
    }
    /*================================================================*/
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }
    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                iv_couverture_profil_user.setImageURI(result.getUri());
                try {
                    uploadImg(u.getEmail(),result.getUri());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Erreur: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    private boolean uploadImg(final String email, final Uri uri) throws FileNotFoundException {
        String phot = uri.getPath();
        Bitmap bitmap = ImageLoader.init().from(phot).requestSize(2048, 2048).getBitmap();
        String encodedImage = ImageBase64.encode(bitmap);
        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put("image", encodedImage);

        PostResponseAsyncTask task = new PostResponseAsyncTask(ProfileUser.this, postData, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                final ProgressDialog progressDialog = new ProgressDialog(ProfileUser.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Téléchargement en cours...");
                progressDialog.show();
                new CountDownTimer(3000,1000){

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        u = get_user(email);
                        if (u.getId() != 0){
                            String link_couverture = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/couverture/"+u.getCouverture();
                            Glide.with(ProfileUser.this).load(uri).fitCenter().crossFade().into(iv_couverture_profil_user);
                            progressDialog.dismiss();
                        }
                    }
                }.start();
            }
        });
        task.execute("http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/upload_couverture.php?email="+email);

        task.setEachExceptionsHandler(new EachExceptionsHandler() {
            @Override
            public void handleIOException(IOException e) {
            }

            @Override
            public void handleMalformedURLException(MalformedURLException e) {
                Toast.makeText(getApplicationContext(), "URL Error.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleProtocolException(ProtocolException e) {
                Toast.makeText(getApplicationContext(), "Protocol Error.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleUnsupportedEncodingException(UnsupportedEncodingException e) {
                Toast.makeText(getApplicationContext(), "Encoding Error.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            Intent intent = new Intent(this,Home.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        return super.onOptionsItemSelected(item);
    }

    private long lastPressedTime;
    private static final int PERIOD = 2000;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(this,Home.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }
    /*=========================================================================*/
    private Users get_user(String email){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/get_user.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setIndicatif(jsonObject.optString("indicatif"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
    //La méthode qui modifie la ville
    private void manage_ville(String ville,String email){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/manage_ville.php?ville="+ville+"&email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //La méthode qui modifie le pays
    private void manage_pays(String pays,String email){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/manage_pays.php?pays="+pays+"&email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //La méthode qui modifie le jumero de téléphone
    private void manage_tel(String indicatif,String tel,String email){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/manage_telephone.php?indicatif=+"+indicatif+"&tel="+tel+"&email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
