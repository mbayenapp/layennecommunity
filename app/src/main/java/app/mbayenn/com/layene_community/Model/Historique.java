package app.mbayenn.com.layene_community.Model;

/**
 * Created by bfali on 27/01/2017.
 */

public class Historique {
    private int id;
    private String content;

    public Historique() {
    }

    public Historique(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
