package app.mbayenn.com.layene_community.Activity;

import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Model.Khalifs;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;
import app.mbayenn.library.radio.RadioManager;

public class See_khalif extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private Khalifs kh;
    private ImageView img_close,img_see_more_khalif;
    private TextView txt_nom,txt_khalifat,txt_description;
    private static final String TAG = See_khalif.class.getSimpleName();
    private ConnexionInternet connexionInternet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see__khalif);

        final Bundle extras = getIntent().getExtras();
        final int id = extras.getInt("id");
        final  String noom = extras.getString("nom");
        final String date_in = extras.getString("date_in");
        final String date_out = extras.getString("date_out");
        final String description = extras.getString("description");
        final String photo = extras.getString("photo");
        toolbar = (Toolbar)findViewById(R.id.toolbar_see_more_article);
        /*==============================================================*/
        img_close = (ImageView)findViewById(R.id.img_cancel_khalif);
        img_see_more_khalif = (ImageView)findViewById(R.id.img_see_more_khalif);
        txt_nom = (TextView)findViewById(R.id.txt_titre_see_more_khalif);
        txt_khalifat = (TextView)findViewById(R.id.txt_khalifat);
        txt_description = (TextView)findViewById(R.id.txt_description_see_more_khalif);
        /*==============================================================*/
        /*==============================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(See_khalif.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(See_khalif.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/khalifs/" + photo;
            Glide.with(getApplicationContext()).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_see_more_khalif);
            txt_nom.setText(noom);
            if (!date_out.equals("null")) {
                txt_khalifat.setText(date_in + " - " + date_out);
            } else {
                txt_khalifat.setText("Khalif actuel");
            }
            txt_description.setText(description);

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
                }
            });
        }
    }
    /*=======================================================================*/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
        }
        return false;
    }
    //La méthode qui retourne un khalif
    private Khalifs get_khalif(int id){
        Khalifs kh = new Khalifs();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Khalifs/search_khalif.php?id="+id+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            Log.d(TAG,"Khalif: "+result);
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                kh.setId(jsonObject.optInt("id"));
                kh.setNom(jsonObject.optString("nom"));
                kh.setDate_in(jsonObject.optString("date_in"));
                kh.setDate_out(jsonObject.optString("date_out"));
                kh.setDescription(jsonObject.optString("description"));
                kh.setPhoto(jsonObject.optString("photo"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return kh;
    }
}
