package app.mbayenn.com.layene_community.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Activity.See_More_Article;
import app.mbayenn.com.layene_community.Adapter.Article_Adapter;
import app.mbayenn.com.layene_community.Model.Articles;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;
import app.mbayenn.com.layene_community.Utils.Verification_Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Home_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Home_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private String token = "layenne2017";
    private ScrollView scroll_home;
    private ImageView img_last_article,img_profil_home;
    private TextView txt_moderateur,txt_titre,txt_content,txt_date;
    private ListView lst_articles;
    private Article_Adapter adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<Articles> articles;
    private Articles article;
    private CardView card_last_article;
    private ConnexionInternet connexionInternet;



    public Home_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Home_Fragment newInstance(String param1, String param2) {
        Home_Fragment fragment = new Home_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        scroll_home = (ScrollView)view.findViewById(R.id.scroll_home);
        img_last_article = (ImageView)view.findViewById(R.id.img_last_new);
        txt_date = (TextView)view.findViewById(R.id.txt_date_new);
        txt_moderateur = (TextView)view.findViewById(R.id.txt_moderateur);
        txt_titre = (TextView)view.findViewById(R.id.txt_titre_new);
        txt_content = (TextView)view.findViewById(R.id.txt_content_new);
        lst_articles = (ListView)view.findViewById(R.id.lst_articles);
        card_last_article = (CardView)view.findViewById(R.id.card_last_article);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipeRefreshLayout);
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            /*==================================================================*/
            articles = get_Articles();
            if (!articles.isEmpty()) {
                mSwipeRefreshLayout.setOnRefreshListener(this);
                mSwipeRefreshLayout.setColorSchemeColors(Color.parseColor("#01315a"));
                mSwipeRefreshLayout.setRefreshing(true);
            /*==================================================================*/
                new CountDownTimer(2000, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (get_Last_Article() != null) {
                            article = get_Last_Article();
                        }
                        String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/articles/" + article.getPhoto();
                        Glide.with(Home_Fragment.this).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_last_article);
                        txt_moderateur.setText("par : " + article.getPrenom() + " " + article.getNom());
                        txt_titre.setText(article.getTitre());
                        txt_content.setText(article.getContent());
                        Verification_Date vd = new Verification_Date();
                        String d = vd.get_date(article.getDate_sent());
                        txt_date.setText(d);
                        adapter = new Article_Adapter(getActivity(), articles);
                        lst_articles.setAdapter(adapter);
                        lst_articles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Articles article = articles.get(position);
                                Intent intent = new Intent(getActivity(), See_More_Article.class);
                                intent.putExtra("id_article", article.getId());
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                            }
                        });
                        setListViewHeightBasedOnChildren(lst_articles);

                        new CountDownTimer(3000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                mSwipeRefreshLayout.setRefreshing(false);
                                scroll_home.setVisibility(View.VISIBLE);
                            }
                        }.start();
                    }
                }.start();
                card_last_article.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), See_More_Article.class);
                        intent.putExtra("id_article", article.getId());
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                    }
                });
            }
        }

        return view;
    }
    //Modifier la hauteur de mon listView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*=======================================================================*/
    //La méthode qui retourne le dernier article
    private Articles get_Last_Article(){
        Articles article = new Articles();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Articles/last_article.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                article.setId(jsonObject.optInt("id"));
                article.setDate_sent(jsonObject.optString("date_sent"));
                article.setTitre(jsonObject.optString("titre"));
                article.setContent(jsonObject.optString("content"));
                article.setPhoto(jsonObject.optString("photo"));
                article.setSource(jsonObject.optString("source"));
                article.setNom(jsonObject.optString("nom"));
                article.setPrenom(jsonObject.optString("prenom"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return article;
    }
    //La méthode qui retourne les autres articles
    private List<Articles> get_Articles(){
        List<Articles> articles = new ArrayList<>();
        Articles article;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Articles/list_articles.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length();i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    article = new Articles();
                    article.setId(jsonObject.optInt("id"));
                    article.setDate_sent(jsonObject.optString("date_sent"));
                    article.setTitre(jsonObject.optString("titre"));
                    article.setContent(jsonObject.optString("content"));
                    article.setPhoto(jsonObject.optString("photo"));
                    article.setSource(jsonObject.optString("source"));
                    article.setNom(jsonObject.optString("nom"));
                    article.setPrenom(jsonObject.optString("prenom"));
                    articles.add(article);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                article = get_Last_Article();
                String link_img ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/articles/"+article.getPhoto();
                Glide.with(Home_Fragment.this).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_last_article);
                txt_moderateur.setText("par : "+article.getPrenom()+" "+article.getNom());
                txt_titre.setText(article.getTitre());
                txt_content.setText(article.getContent());
                Verification_Date vd = new Verification_Date();
                String d = vd.get_date(article.getDate_sent());
                txt_date.setText(d);
                articles.clear();
                articles.addAll(get_Articles());
                adapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },3000);
    }
}
