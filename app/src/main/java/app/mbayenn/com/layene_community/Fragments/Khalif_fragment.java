package app.mbayenn.com.layene_community.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Activity.See_khalif;
import app.mbayenn.com.layene_community.Adapter.Khalif_Adapter;
import app.mbayenn.com.layene_community.Model.Khalifs;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Khalif_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Khalif_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Khalif_fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String token = "layenne2017";
    private String mParam1;
    private String mParam2;
    private SwipeRefreshLayout swipe_khalif;
    private ListView list_khalif;
    private List<Khalifs> khalifs = get_Khalifs();
    private Khalif_Adapter adapter;
    private ConnexionInternet connexionInternet;

    private OnFragmentInteractionListener mListener;

    public Khalif_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Khalif_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Khalif_fragment newInstance(String param1, String param2) {
        Khalif_fragment fragment = new Khalif_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_khalif, container, false);
        swipe_khalif = (SwipeRefreshLayout)view.findViewById(R.id.swipeKhalif);
        list_khalif = (ListView)view.findViewById(R.id.list_khalif);

        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            swipe_khalif.setOnRefreshListener(this);
            swipe_khalif.setColorSchemeColors(Color.parseColor("#01315a"));
            swipe_khalif.setRefreshing(true);
            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    adapter = new Khalif_Adapter(getContext(), khalifs);
                    list_khalif.setAdapter(adapter);
                    swipe_khalif.setRefreshing(false);
                }
            }.start();
            list_khalif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final Khalifs kh = khalifs.get(i);
                    Intent intent = new Intent(getContext(), See_khalif.class);
                    intent.putExtra("id", kh.getId());
                    intent.putExtra("nom", kh.getNom());
                    intent.putExtra("date_in", kh.getDate_in());
                    intent.putExtra("date_out", kh.getDate_out());
                    intent.putExtra("description", kh.getDescription());
                    intent.putExtra("photo", kh.getPhoto());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                }
            });
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_khalif.postDelayed(new Runnable() {
            @Override
            public void run() {
                khalifs.clear();
                khalifs.addAll(get_Khalifs());
                adapter.notifyDataSetChanged();
                swipe_khalif.setRefreshing(false);
            }
        },2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //La méthode qui retourne la liste des khalifs généraux
    private List<Khalifs> get_Khalifs(){
        List<Khalifs> khalifs = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Khalifs/list_khalif.php?token="+token;
        Khalifs kh;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    kh = new Khalifs();
                    kh.setId(jsonObject.optInt("id"));
                    kh.setNom(jsonObject.optString("nom"));
                    kh.setDate_in(jsonObject.optString("date_in"));
                    kh.setDate_out(jsonObject.optString("date_out"));
                    kh.setDescription(jsonObject.optString("description"));
                    kh.setPhoto(jsonObject.optString("photo"));
                    khalifs.add(kh);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return khalifs;
    }
}
