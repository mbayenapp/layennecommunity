package app.mbayenn.com.layene_community.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Activity.See_More_Article;
import app.mbayenn.com.layene_community.Model.Articles;
import app.mbayenn.com.layene_community.Model.Articles;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.Verification_Date;

/**
 * Created by bfali on 13/11/2016.
 */

public class Article_Adapter extends BaseAdapter {
    private Activity activity;
    private List<Articles> articlesItems;
    private LayoutInflater inflater;

    public Article_Adapter(Activity activity, List<Articles> articlesItems) {
        this.activity = activity;
        this.articlesItems = articlesItems;
    }

    @Override
    public int getCount() {
        return articlesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return articlesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.article_item,null);
        ImageView img_article = (ImageView)convertView.findViewById(R.id.img_article);
        TextView txt_date = (TextView)convertView.findViewById(R.id.txt_date_article);
        TextView txt_moderateur = (TextView)convertView.findViewById(R.id.txt_moderateur_article);
        TextView txt_titre = (TextView)convertView.findViewById(R.id.txt_titre_article);
        TextView txt_content = (TextView)convertView.findViewById(R.id.txt_content_article);
        CardView card_article = (CardView)convertView.findViewById(R.id.card_article);
        final Articles article = articlesItems.get(position);
        String link_img ="http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/articles/"+article.getPhoto();
        Glide.with(activity).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_article);
        txt_moderateur.setText("par : "+article.getPrenom()+" "+article.getNom());
        txt_titre.setText(article.getTitre());
        txt_content.setText(article.getContent());
        Verification_Date vd = new Verification_Date();
        String d = vd.get_date(article.getDate_sent());
        txt_date.setText(d);
        /*card_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity,See_More_Article.class);
                intent.putExtra("id_article",article.getId());
                activity.startActivity(intent);
            }
        });*/
        return convertView;
    }
}
