package app.mbayenn.com.layene_community.Model;

/**
 * Created by bfali on 03/12/2016.
 */

public class Users {
    private int id,code_postal;
    private String nom,prenom,adresse,sexe,indicatif,tel,email,password,ville,profil,couverture,pays,etat,statut;

    public Users() {
    }

    public Users(String email) {
        this.email = email;
    }

    public Users(int id, String nom, String prenom, String adresse, String sexe, String indicatif, String tel, String email, String password, String ville, int code_postal, String profil, String couverture, String pays, String etat, String statut) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.sexe = sexe;
        this.indicatif = indicatif;
        this.tel = tel;
        this.email = email;
        this.password = password;
        this.ville = ville;
        this.code_postal = code_postal;
        this.profil = profil;
        this.couverture = couverture;
        this.pays = pays;
        this.etat = etat;
        this.statut = statut;
    }

    public Users(String nom, String prenom, String adresse, String email, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.email = email;
        this.password = password;
    }

    public Users(String sexe,String indicatif, String tel, String ville, int code_postal, String pays,String email) {
        this.sexe = sexe;
        this.tel = tel;
        this.ville = ville;
        this.code_postal = code_postal;
        this.pays = pays;
        this.email = email;
        this.indicatif = indicatif;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(int code_postal) {
        this.code_postal = code_postal;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public String getCouverture() {
        return couverture;
    }

    public void setCouverture(String couverture) {
        this.couverture = couverture;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
}
