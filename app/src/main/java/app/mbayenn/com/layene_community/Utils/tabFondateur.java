package app.mbayenn.com.layene_community.Utils;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Model.Fondateur;
import app.mbayenn.com.layene_community.R;

/**
 * Created by bfali on 27/01/2017.
 */

public class tabFondateur extends Fragment {
    private String token = "layenne2017";
    private TextView txt_titre,txt_description;
    private Fondateur f = get_fondateur();
    private ConnexionInternet connexionInternet;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabfondateur, container, false);
        txt_description = (TextView)rootView.findViewById(R.id.txt_description_fondateur);
        txt_titre = (TextView)rootView.findViewById(R.id.txt_titre_fondateur);
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            txt_titre.setText(f.getTitre());
            txt_description.setText(f.getDescription());
        }
        return rootView;
    }
    /*===========================================================*/
    //La méthode qui retourne le fondateur
    private Fondateur get_fondateur(){
        Fondateur fondateur = new Fondateur();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/static/get_fondateur.php?token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                fondateur.setId(jsonObject.optInt("id"));
                fondateur.setTitre(jsonObject.optString("titre"));
                fondateur.setDescription(jsonObject.optString("description"));
                fondateur.setImage(jsonObject.optString("image"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fondateur;
    }
}
