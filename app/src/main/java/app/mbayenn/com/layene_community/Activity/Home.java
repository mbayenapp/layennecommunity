package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tyorikan.voicerecordingvisualizer.RecordingSampler;
import com.tyorikan.voicerecordingvisualizer.VisualizerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Fragments.Compagnon;
import app.mbayenn.com.layene_community.Fragments.Home_Fragment;
import app.mbayenn.com.layene_community.Fragments.Issa;
import app.mbayenn.com.layene_community.Fragments.Khalif_fragment;
import app.mbayenn.com.layene_community.Fragments.Sermon;
import app.mbayenn.com.layene_community.Fragments.Site;
import app.mbayenn.com.layene_community.Helper.SessionManagerRadio;
import app.mbayenn.com.layene_community.Helper.SessionManagerUser;
import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.library.radio.RadioListener;
import app.mbayenn.library.radio.RadioManager;
import de.hdodenhof.circleimageview.CircleImageView;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,RadioListener, RecordingSampler.CalculateVolumeListener {
    private String token = "layenne2017";
    String title ="";
    Fragment fragment = null;
   // private final String[] RADIO_URL = {"http://radios.xalimasn.com:9016/listen.pls"};
    private final String[] RADIO_URL = {"http://173.236.30.50:9010/;"};
    //http://rockfm.rockfm.com.tr:9450
    /*http://radios.xalimasn.com:9016/listen.pls*/
    private static final String TAG = Home.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar p_start_radio;
    private CircleImageView img_start_radio;
    private VisualizerView visualizer;
    private Bitmap imgStart,imgStop;
    RadioManager mRadioManager;
    private RecordingSampler recordingSampler;
    private SessionManagerRadio sessionManagerRadio;
    private View navHeader;
    private NavigationView navigationView;
    private SessionManagerUser sessionManagerUser;
    private String link = "";
    private Users u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("SUR LE FIL");
        setSupportActionBar(toolbar);
        //Mon fragment par défaut
        title = getString(R.string.title_home);
        fragment = new Home_Fragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(title);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Tv.class));
                overridePendingTransition(R.anim.push_top_in,R.anim.push_top_out);
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        /*=====================================================*/
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        final CircleImageView img_profil_home = (CircleImageView) navHeader.findViewById(R.id.img_profil_home);
        final TextView txt_name_home = (TextView)navHeader.findViewById(R.id.txt_name_profil_home);
        final TextView txt_email_home = (TextView)navHeader.findViewById(R.id.txt_email_profil_home);
        final LinearLayout l_user_on_line = (LinearLayout)navHeader.findViewById(R.id.l_user_on_line);
        final LinearLayout l_user_off_line = (LinearLayout)navHeader.findViewById(R.id.l_user_off_line);
        p_start_radio = (ProgressBar)findViewById(R.id.progress_start_radio);
        img_start_radio = (CircleImageView)findViewById(R.id.img_start_radio);
        visualizer = (VisualizerView) findViewById(R.id.visualizer_home);
        Menu m = (Menu)navigationView.getMenu();
        final MenuItem login = (MenuItem)m.findItem(R.id.nav_compte);
        final MenuItem logout = (MenuItem)m.findItem(R.id.nav_logout);
        //Initialisation du visualiser
        recordingSampler = new RecordingSampler();
        recordingSampler.setVolumeListener(this);
        recordingSampler.setSamplingInterval(100);
        recordingSampler.link(visualizer);
        //Session de ma radio
        sessionManagerRadio = new SessionManagerRadio(this);
        /*A enlever par la suite*/
        sessionManagerRadio.clearStatutRadion();
        //Initialisation de la radio
        mRadioManager = RadioManager.with(getApplicationContext());
        mRadioManager.registerListener(this);
        mRadioManager.setLogging(true);
        if (sessionManagerRadio.getStatutRadio().equals("on")){
            if (!mRadioManager.isPlaying()) {
                mRadioManager.startRadio(RADIO_URL[0]);
            }
            imgStop = BitmapFactory.decodeResource(getResources(), R.drawable.ic_pause_radio_18dp);
            img_start_radio.setImageBitmap(imgStop);
            //recordingSampler.startRecording();
        }
        //Initialiser la radio
        initializeUI();
        /*====================================================================*/
        sessionManagerUser = new SessionManagerUser(this);
        //La session de l'utilisateur
        if (!sessionManagerUser.getLoggedInUser().getEmail().equals("")){
            login.setVisible(false);
            logout.setVisible(true);
            l_user_on_line.setVisibility(View.VISIBLE);
            l_user_off_line.setVisibility(View.GONE);
            u = get_user(sessionManagerUser.getLoggedInUser().getEmail());
            if (!u.getEmail().equals("null")){
                link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/"+u.getProfil();
            }else {
                link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/boy.png";
            }
            Glide.with(getApplicationContext()).load(link).placeholder(R.drawable.boy).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil_home);
            txt_name_home.setText(u.getPrenom()+" "+u.getNom());
            txt_email_home.setText(u.getEmail());
        }else {
            login.setVisible(true);
            logout.setVisible(false);
        }
        l_user_on_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawers();
                Intent intent = new Intent(getApplicationContext(),ProfileUser.class);
                intent.putExtra("email",u.getEmail());
                intent.putExtra("profil", u.getProfil());
                intent.putExtra("nom",u.getPrenom()+" "+u.getNom());
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    private void initializeUI() {
        img_start_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mRadioManager.isPlaying()) {
                    mRadioManager.startRadio(RADIO_URL[0]);
                    imgStop = BitmapFactory.decodeResource(getResources(), R.drawable.ic_pause_radio_18dp);
                    img_start_radio.setImageBitmap(imgStop);
                    //visualizer.startListening();
                    sessionManagerRadio.storeStatutRadio("on");
                    //recordingSampler.startRecording();
                }else {
                    mRadioManager.stopRadio();
                    imgStart = BitmapFactory.decodeResource(getResources(), R.drawable.ic_play_arrow_radio_18dp);
                    img_start_radio.setImageBitmap(imgStart);
                    //sessionManagerRadio.clearUserData();
                    //visualizer.stopListening();
                    sessionManagerRadio.clearStatutRadion();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            fragment = new Home_Fragment();
            title = getString(R.string.title_home);
        } else if (id == R.id.nav_tv) {
            startActivity(new Intent(getApplicationContext(),Tv.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.nav_video) {
            startActivity(new Intent(getApplicationContext(),Videotheque.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }else if (id == R.id.nav_confrerie){
            startActivity(new Intent(getApplicationContext(),Confrerie.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }else if (id == R.id.nav_sites){
            fragment = new Site();
            title = getString(R.string.txt_site);
        }else if (id == R.id.nav_issa){
            fragment = new Issa();
            title = getString(R.string.txt_issa);
        }else if (id == R.id.nav_khalif){
            fragment = new Khalif_fragment();
            title = getString(R.string.txt_khalif);
        }else if (id == R.id.nav_compagnon){
            fragment = new Compagnon();
            title = getString(R.string.txt_compagon);
        }else if (id == R.id.nav_sermon){
            fragment = new Sermon();
            title = getString(R.string.txt_sermon);
        } else if (id == R.id.nav_setting) {

        }else if (id == R.id.nav_suggestion) {
            startActivity(new Intent(getApplicationContext(),Contact.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.nav_info) {
            startActivity(new Intent(getApplicationContext(),Mention.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }  else if (id == R.id.nav_compte) {
            startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else if (id == R.id.nav_logout) {
            final String rep = logout(sessionManagerUser.getLoggedInUser().getEmail());
            if (rep.equals("La session a été bien déconnectée")) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Home.this);
                alertDialogBuilder.setTitle("Confirmer la déconnexion");
                alertDialogBuilder.setMessage("Voulez vous vraiment se déconnecter? Si oui, vous devrez à nouveau saisir votre email et votre mot de passe lors de votre prochaine connexion !");

                final ProgressDialog dialog = new ProgressDialog(this,R.style.AppTheme_Dark_Dialog);
                alertDialogBuilder.setPositiveButton("Se déconnecter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        dialog.setMessage("Déconnexion en cours...");
                        // dialog.setCancelable(false);
                        dialog.setIndeterminate(false);
                        dialog.show();
                        new CountDownTimer(2000, 500) {

                            public void onTick(long millisUntilFinished) {
                                //nothing
                            }

                            public void onFinish() {
                    /*if (user != null) {
                        mAuth.signOut();
                    }else {*/
                                sessionManagerUser.clearUserData();
                                //}
                                startActivity(new Intent(getApplicationContext(), Home.class));
                                dialog.dismiss();
                                finish();
                            }
                        }.start();
                    }
                });
                alertDialogBuilder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else {
                Toast.makeText(getApplicationContext(),rep,Toast.LENGTH_SHORT).show();
            }
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Home.this);
            alertDialogBuilder.setMessage("Voulez vous quitter l'application?");

            alertDialogBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });
            alertDialogBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        return false;
    }
    public void onStart() {
        //visualizer.startListening();
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onDestroy() {
        /*if (recordingSampler.isRecording()) {
            recordingSampler.release();
        }*/
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRadioManager.connect();
    }

    @Override
    public void onRadioLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                img_start_radio.setVisibility(View.GONE);
                p_start_radio.setVisibility(View.VISIBLE);
                /*new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (!mRadioManager.isPlaying()){
                            Toast.makeText(getApplicationContext(),"Aucun flux radio n'a été trouvé",Toast.LENGTH_SHORT).show();
                            imgStart = BitmapFactory.decodeResource(getResources(), R.drawable.ic_play_arrow_radio_18dp);
                            img_start_radio.setImageBitmap(imgStart);
                            img_start_radio.setVisibility(View.VISIBLE);
                            p_start_radio.setVisibility(View.GONE);
                        }
                    }
                }, 8000);*/
            }
        });
    }

    @Override
    public void onRadioConnected() {

    }

    @Override
    public void onRadioStarted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgStop = BitmapFactory.decodeResource(getResources(), R.drawable.ic_pause_radio_18dp);
                img_start_radio.setImageBitmap(imgStop);
                img_start_radio.setVisibility(View.VISIBLE);
                p_start_radio.setVisibility(View.GONE);
                //recordingSampler.startRecording();
               /* if (!recordingSampler.isRecording()) {
                    recordingSampler.startRecording();
                }*/
            }
        });
    }

    @Override
    public void onRadioStopped() {
        imgStart = BitmapFactory.decodeResource(getResources(), R.drawable.ic_play_arrow_radio_18dp);
        img_start_radio.setImageBitmap(imgStart);
        /*if (recordingSampler.isRecording()) {
            recordingSampler.stopRecording();
        }*/
        sessionManagerRadio.clearStatutRadion();
    }

    @Override
    public void onMetaDataReceived(String s, String s2) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onCalculateVolume(int volume) {

    }
    /*=========================================================================*/
    //la fonction qui permet de faire une déconnexion7
    private String logout(String email){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/logout.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    //La fonction qui récupére les informations de l'utilisateurs dont l'email est fournie en paremètre
    private Users get_user(String email){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/get_user.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setIndicatif(jsonObject.optString("indicatif"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
