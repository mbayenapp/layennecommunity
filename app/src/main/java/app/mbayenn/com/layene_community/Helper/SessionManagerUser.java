package app.mbayenn.com.layene_community.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.Model.Users;

/**
 * Created by bfali on 04/12/2016.
 */

public class SessionManagerUser {
    private static String TAG = SessionManagerUser.class.getSimpleName();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "UserDetails";
    private static final String KEY_IS_LOGGEDIN = "statut";

    public SessionManagerUser(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }
    public void storeUser(Users user){
        editor = pref.edit();
        editor.putString("email",user.getEmail());
        editor.commit();
    }
    public Users getLoggedInUser(){
        String email = pref.getString("email","");
        Users user = new Users(email);
        return user;
    }
    public void setLoggedInUser(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearUserData(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        Users p = getLoggedInUser();
        boolean statut = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (p.getEmail() != ""){
            statut = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return statut;
    }

}
