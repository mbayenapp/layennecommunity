package app.mbayenn.com.layene_community.Activity;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

public class See_site extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private ImageView img_close,img_see_site;
    private TextView txt_title,txt_description;
    private ConnexionInternet connexionInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_site);
        /*=======================================================*/
        final Bundle extras = getIntent().getExtras();
        final String titre = extras.getString("titre");
        final  String description = extras.getString("description");
        final String image = extras.getString("image");
        toolbar = (Toolbar)findViewById(R.id.toolbar_see_site);
        /*==============================================================*/
        img_close = (ImageView)findViewById(R.id.img_cancel_site);
        img_see_site= (ImageView)findViewById(R.id.img_see_site);
        txt_title = (TextView)findViewById(R.id.txt_titre_see_site);
        txt_description = (TextView)findViewById(R.id.txt_description_see_site);
        /*=======================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(See_site.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(See_site.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/sites/" + image;
            Glide.with(getApplicationContext()).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_see_site);
            txt_title.setText(titre);
            txt_description.setText(description);
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
                }
            });
        }
    }
}
