package app.mbayenn.com.layene_community.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import app.mbayenn.com.layene_community.R;

public class Mention extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private LinearLayout l_progress,l_webView;
    private WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mention);
        toolbar = (Toolbar)findViewById(R.id.toolbar_mention);
        toolbar.setTitle("Mentions légales");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        l_progress = (LinearLayout)findViewById(R.id.l_progress_mention);
        l_webView = (LinearLayout)findViewById(R.id.l_web_mention);
        web = (WebView)findViewById(R.id.web_mention);
        web.loadUrl("http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/mentions.php");
        web.setHorizontalScrollBarEnabled(false);
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                l_progress.setVisibility(View.GONE);
                l_webView.setVisibility(View.VISIBLE);
            }
        }.start();
    }
    /*=======================================================================*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
            overridePendingTransition(R.anim.push_bottom_in,R.anim.push_bottom_out);
        }
        return false;
    }
}
