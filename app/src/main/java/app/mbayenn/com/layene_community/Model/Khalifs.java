package app.mbayenn.com.layene_community.Model;

/**
 * Created by bfali on 29/01/2017.
 */

public class Khalifs {
    private int id;
    private String nom,date_in,date_out,description,photo;

    public Khalifs() {
    }

    public Khalifs(int id, String nom, String date_in, String date_out, String description, String photo) {
        this.id = id;
        this.nom = nom;
        this.date_in = date_in;
        this.date_out = date_out;
        this.description = description;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDate_in() {
        return date_in;
    }

    public void setDate_in(String date_in) {
        this.date_in = date_in;
    }

    public String getDate_out() {
        return date_out;
    }

    public void setDate_out(String date_out) {
        this.date_out = date_out;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
