package app.mbayenn.com.layene_community.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Commentaires;
import app.mbayenn.com.layene_community.Utils.Verification_Date;
import app.mbayenn.com.layene_community.Model.Commentaires;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.Verification_Date;

/**
 * Created by bfali on 04/12/2016.
 */

public class Comments_Adapter extends BaseAdapter{
    private Activity activity;
    private List<Commentaires> commentsItems;
    private LayoutInflater inflater;

    public Comments_Adapter(Activity activity, List<Commentaires> commentsItems) {
        this.activity = activity;
        this.commentsItems = commentsItems;
    }

    @Override
    public int getCount() {
        return commentsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return commentsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.comment_item,null);
        TextView txt_date = (TextView)convertView.findViewById(R.id.txt_date_comment);
        TextView txt_user = (TextView)convertView.findViewById(R.id.txt_user_comment);
        TextView txt_content = (TextView)convertView.findViewById(R.id.txt_content_comment);
        final Commentaires comment = commentsItems.get(position);
        txt_user.setText(comment.getFirst_name()+" "+comment.getLast_name());
        txt_content.setText(comment.getContent());
        Verification_Date vd = new Verification_Date();
        String d = vd.get_date(comment.getDate_comment());
        txt_date.setText(d);
        return convertView;
    }
}
