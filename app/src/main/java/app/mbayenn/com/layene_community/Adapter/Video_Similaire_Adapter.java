package app.mbayenn.com.layene_community.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.Model.Videos;
import app.mbayenn.com.layene_community.R;
import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by bfali on 15/11/2016.
 */

public class Video_Similaire_Adapter extends BaseAdapter {
    private Activity activity;
    private List<Videos> videosItems;
    private LayoutInflater inflater;

    public Video_Similaire_Adapter(Activity activity, List<Videos> videosItems) {
        this.activity = activity;
        this.videosItems = videosItems;
    }

    @Override
    public int getCount() {
        return videosItems.size();
    }

    @Override
    public Object getItem(int position) {
        return videosItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.video_similaire_item,null);
        ImageView img_video = (ImageView)convertView.findViewById(R.id.img_video_similaire);
        TextView txt_titre = (TextView)convertView.findViewById(R.id.txt_titre_video_similaire);
        Videos video = videosItems.get(position);
        String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/videos/"+video.getImage();
        Glide.with(activity).load(link_img)
                .centerCrop()
                .crossFade()
                .bitmapTransform(new BlurTransformation(activity,10))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_video);
        txt_titre.setText(video.getTitre());
        return convertView;
    }
}
