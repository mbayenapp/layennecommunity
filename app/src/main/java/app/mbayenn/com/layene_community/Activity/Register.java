package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;

public class Register extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private EditText txt_nom,txt_prenom,txt_adresse,txt_email,txt_password,txt_repeat;
    private Button btn_register;
    private ProgressDialog progressDialog;
    private String userId;
    private static final String TAG = Register.class.getSimpleName();
    private Users userI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        /*========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_register);
        toolbar.setTitle("S'inscrire");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        txt_nom = (EditText)findViewById(R.id.nom);
        txt_prenom = (EditText)findViewById(R.id.prenom);
        txt_adresse = (EditText)findViewById(R.id.adresse);
        txt_email = (EditText)findViewById(R.id.mail);
        txt_password = (EditText)findViewById(R.id.newpassword);
        txt_repeat = (EditText)findViewById(R.id.repeatpassword);
        btn_register = (Button)findViewById(R.id.btn_inscription);
        /*========================================================*/
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nom = txt_nom.getText().toString().trim();
                final String prenom = txt_prenom.getText().toString().trim();
                final String adresse = txt_adresse.getText().toString().trim();
                final String email = txt_email.getText().toString().trim();
                final String password = txt_password.getText().toString().trim();
                String repeat = txt_repeat.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (!nom.equals("") || !prenom.equals("") || !adresse.equals("") || !email.equals("") || !password.equals("")){
                    if (password.length() >= 6){
                        if (password.equals(repeat)){
                            if(email.matches(emailPattern)) {
                                //userInformation = new UserInformation(nom,prenom,adresse,email,password,"","","","","","","");
                                //CreateUser(userInformation);
                                userI = new Users(nom,prenom,adresse,email,password);
                                String reponse = addUser(userI);
                                if (reponse.equals("ok")){
                                    final ProgressDialog progressDialog = new ProgressDialog(Register.this,
                                            R.style.AppTheme_Dark_Dialog);
                                    progressDialog.setIndeterminate(true);
                                    progressDialog.setMessage("Enregistrement en cours...");
                                    progressDialog.show();
                                    new CountDownTimer(3000,1000){

                                        @Override
                                        public void onTick(long millisUntilFinished) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            progressDialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(),Complete_profil.class);
                                            intent.putExtra("nom",nom);
                                            intent.putExtra("prenom",prenom);
                                            intent.putExtra("adressse",adresse);
                                            intent.putExtra("email",email);
                                            intent.putExtra("password",password);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                            finish();
                                        }
                                    }.start();
                                }else {
                                    Toast.makeText(getApplicationContext(),reponse,Toast.LENGTH_LONG).show();
                                }
                            }else {
                                Toast.makeText(getApplicationContext(),"Cette adresse email n'est pas valide!", Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(),"Les deux mots de passes ne correspondent!", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Le mot de passe entré est trop court!", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Tous les champs doivent être remplis!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    /*==============================================================*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
        return false;
    }
    //La méthode qui ajoute un utilisateur
    private String addUser(Users user){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/add_compte.php?nom="+user.getNom()+"&prenom="+user.getPrenom()+"&adresse="+ URLEncoder.encode(user.getAdresse())+"&email="+user.getEmail()+"&password="+user.getPassword()+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
