package app.mbayenn.com.layene_community.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import app.mbayenn.com.layene_community.Activity.See_site;
import app.mbayenn.com.layene_community.Adapter.Site_Adapter;
import app.mbayenn.com.layene_community.Model.Sites;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Site.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Site#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Site extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String token = "layenne2017";
    private String mParam1;
    private String mParam2;
    private SwipeRefreshLayout swipe_site;
    private ListView list_site;
    private List<Sites> sites;
    private Site_Adapter adapter;
    private static final String TAG = Site.class.getSimpleName();
    private ConnexionInternet connexionInternet;

    private OnFragmentInteractionListener mListener;

    public Site() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Site.
     */
    // TODO: Rename and change types and number of parameters
    public static Site newInstance(String param1, String param2) {
        Site fragment = new Site();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_site, container, false);
        swipe_site = (SwipeRefreshLayout)view.findViewById(R.id.swipeSite);
        list_site = (ListView)view.findViewById(R.id.list_site);

        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            swipe_site.setOnRefreshListener(this);
            swipe_site.setColorSchemeColors(Color.parseColor("#01315a"));
            swipe_site.setRefreshing(true);
            sites = getSites();
            new CountDownTimer(2000, 1000) {

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    adapter = new Site_Adapter(getContext(), sites);
                    list_site.setAdapter(adapter);
                    swipe_site.setRefreshing(false);
                }
            }.start();
            list_site.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final Sites site = sites.get(i);
                    Intent intent = new Intent(getContext(), See_site.class);
                    intent.putExtra("titre", site.getTitre());
                    intent.putExtra("description", site.getDescription());
                    intent.putExtra("image", site.getImage());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.push_top_in, R.anim.push_top_out);
                }
            });
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_site.postDelayed(new Runnable() {
            @Override
            public void run() {
                sites.clear();
                sites.addAll(getSites());
                adapter.notifyDataSetChanged();
                swipe_site.setRefreshing(false);
            }
        },2000);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //La méthode qui retourne la liste des site layenne
    private List<Sites> getSites(){
        List<Sites>  sites = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Sites/list_sites.php?token="+token;
        Sites site;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            Log.d(TAG,"Les sites: "+result);
            if (result != null) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    site = new Sites();
                    site.setId(jsonObject.optInt("id"));
                    site.setTitre(jsonObject.optString("titre"));
                    site.setDescription(jsonObject.optString("description"));
                    site.setImage(jsonObject.optString("image"));
                    sites.add(site);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sites;
    }
}
