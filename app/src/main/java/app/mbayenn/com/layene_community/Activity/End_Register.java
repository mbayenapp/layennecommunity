package app.mbayenn.com.layene_community.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import app.mbayenn.com.layene_community.R;

public class End_Register extends AppCompatActivity {
    private TextView txt_bienvenu_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end__register);
        final Bundle extras = getIntent().getExtras();
        String nom = extras.getString("nom");
        txt_bienvenu_name = (TextView)findViewById(R.id.txt_bienvenu_name);
        txt_bienvenu_name.setText(nom);
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        }.start();
    }
}
