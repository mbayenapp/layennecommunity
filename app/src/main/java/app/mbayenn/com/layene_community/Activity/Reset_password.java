package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class Reset_password extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private LinearLayout l_email_user_forgot,l_txt_user_forgot,l_see_user_forgot,l_rdEmail,l_rdNumber,l_btn_user_forgot;
    private EditText txt_email;
    private Button btn_search,btn_send_code;
    private CircleImageView img_user_see;
    private TextView txt_name_user_see,txt_email_user_see,txt_nothing_compte,txt_number_user_see,txt_email_user_see_again;
    private AwesomeValidation awesomeValidation;
    private String link_img;
    private String act_send = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        /*========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_reset);
        toolbar.setTitle("Ré-initialiser le mot de passe");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        l_email_user_forgot = (LinearLayout)findViewById(R.id.l_email_user_forgot);
        l_txt_user_forgot = (LinearLayout)findViewById(R.id.l_txt_user_forgot);
        l_see_user_forgot = (LinearLayout)findViewById(R.id.l_see_user_forgot);
        l_btn_user_forgot = (LinearLayout)findViewById(R.id.l_btn_user_forgot);
        txt_email = (EditText)findViewById(R.id.email_forgot);
        btn_search = (Button)findViewById(R.id.btn_go_step2);
        btn_send_code = (Button)findViewById(R.id.btn_send_code);
        img_user_see = (CircleImageView)findViewById(R.id.img_user_see);
        txt_name_user_see = (TextView)findViewById(R.id.txt_name_user_see);
        txt_email_user_see = (TextView)findViewById(R.id.txt_email_user_see);
        txt_nothing_compte = (TextView)findViewById(R.id.txt_nothing_compte);
        txt_email_user_see_again = (TextView)findViewById(R.id.txt_email_user_see_again);
        /*========================================================*/
        txt_name_user_see.setTextAppearance(this,R.style.AppTheme_AppBarOverlay);
        awesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        awesomeValidation.addValidation(this,R.id.email_forgot, Patterns.EMAIL_ADDRESS,R.string.emailerror);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                awesomeValidation.validate();
                if (awesomeValidation.validate()) {
                    final ProgressDialog progressDialog = new ProgressDialog(Reset_password.this,
                            R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Patienter un instant, nous recherchons le compte...");
                    progressDialog.show();
                    String email = txt_email.getText().toString().trim();
                    final Users u = get_user(email);
                    new CountDownTimer(3000,1500){

                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            if (u.getId() != 0){
                                if (!u.getProfil().equals("null")) {
                                    link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/" + u.getProfil();
                                }else {
                                    link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/users/boy.png";
                                }
                                Glide.with(Reset_password.this).load(link_img)
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        .fitCenter()
                                        .into(img_user_see);
                                txt_name_user_see.setText(u.getPrenom()+" "+u.getNom());
                                txt_email_user_see.setText(u.getEmail());
                                String[]part = u.getEmail().split("\\@");
                                String em1 = part[0].substring(0,1);
                                String em3 = part[0].substring(4);
                                String gm = part[1];
                                String[] pm = gm.split("\\.");
                                String gm2 = pm[0].substring(2);
                                txt_email_user_see_again.setText(em1+"***"+em3+"@"+"***"+gm2+"."+pm[1]);
                                l_email_user_forgot.setVisibility(View.GONE);
                                l_btn_user_forgot.setVisibility(View.VISIBLE);
                                l_see_user_forgot.setVisibility(View.VISIBLE);
                                l_txt_user_forgot.setVisibility(View.VISIBLE);
                                progressDialog.dismiss();
                            }
                        }
                    }.start();
                }
            }
        });
        //L'action création de compte
        txt_nothing_compte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Register.class));
                overridePendingTransition(R.anim.push_top_in,R.anim.push_top_out);
                finish();
            }
        });
    }
    /*=========================================================================*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
        return false;
    }
    private Users get_user(String email){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/get_user.php?email="+email+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setIndicatif(jsonObject.getString("indicatif"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
