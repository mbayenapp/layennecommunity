package app.mbayenn.com.layene_community.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.mbayenn.com.layene_community.Helper.SessionManagerUser;
import app.mbayenn.com.layene_community.Model.Articles;
import app.mbayenn.com.layene_community.Model.Users;
import app.mbayenn.com.layene_community.R;
import app.mbayenn.com.layene_community.Utils.ConnexionInternet;
import app.mbayenn.com.layene_community.Utils.Verification_Date;

public class See_More_Article extends AppCompatActivity {
    private String token = "layenne2017";
    private Toolbar toolbar;
    private ImageView img_close,img_see_more_article,img_share_article,img_comment;
    private TextView txt_titre,txt_moderateur,txt_date,txt_content;
    private LayoutInflater inflater;
    private AwesomeValidation awesomeValidation;
    private ConnexionInternet connexionInternet;
    private SessionManagerUser sessionManagerUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see__more__article);
        final Bundle extras = getIntent().getExtras();
        final int id_article = extras.getInt("id_article");
        toolbar = (Toolbar)findViewById(R.id.toolbar_see_more_article);
        /*==============================================================*/
        img_close = (ImageView)findViewById(R.id.img_cancel_article);
        img_see_more_article = (ImageView)findViewById(R.id.img_see_more_article);
        txt_titre = (TextView)findViewById(R.id.txt_titre_see_more_article);
        txt_moderateur = (TextView)findViewById(R.id.txt_moderateur_see_more_article);
        txt_date = (TextView)findViewById(R.id.txt_date_more_article);
        txt_content = (TextView)findViewById(R.id.txt_content_see_more_article);
        img_share_article = (ImageView)findViewById(R.id.img_share_article);
        img_comment = (ImageView)findViewById(R.id.img_comment_article);
        /*==============================================================*/
        connexionInternet = new ConnexionInternet();
        sessionManagerUser = new SessionManagerUser(this);
        if (!connexionInternet.isConnectedInternet(See_More_Article.this)){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(See_More_Article.this);
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier votre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
            });
            final Articles article = get_Article(id_article);
            String link_img = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/webroot/articles/" + article.getPhoto();
            Glide.with(See_More_Article.this).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_see_more_article);
            txt_titre.setText(article.getTitre());
            txt_moderateur.setText("Par : " + article.getPrenom() + " " + article.getNom());
            Verification_Date vd = new Verification_Date();
            String d = vd.get_date(article.getDate_sent());
            txt_date.setText(d);
            txt_content.setText(article.getContent());
            img_share_article.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, article.getTitre());
                    share.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>" + article.getContent() + "</p>"));
                    startActivity(Intent.createChooser(share, "Partager cet article!"));
                }
            });
        }
        img_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sessionManagerUser.getLoggedInUser().getEmail().equals("")) {
                    Intent intent = new Intent(getApplicationContext(), Comment.class);
                    intent.putExtra("id_article", id_article);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                } else {
                    inflater = See_More_Article.this.getLayoutInflater();
                    View content = inflater.inflate(R.layout.login_item, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(See_More_Article.this);
                    builder.setView(content).setTitle("Connecter vous")
                            .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    final EditText txt_email = (EditText) content.findViewById(R.id.email);
                    final EditText txt_password = (EditText) content.findViewById(R.id.password);
                    Button btn_login = (Button) content.findViewById(R.id.btn_login);
                    TextView txt_forgot = (TextView) content.findViewById(R.id.txt_forgot_password);
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                    btn_login.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String email = txt_email.getText().toString().trim();
                            String password = txt_password.getText().toString().trim();
                            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                            if (!email.equals("") || !password.equals("")) {
                                if (email.matches(emailPattern)) {
                                    if (password.length() < 6) {
                                        Toast.makeText(getApplicationContext(), "Le mot de passe est trop court!", Toast.LENGTH_LONG).show();
                                    } else {
                                        Users user = get_user(email, password);
                                        if (user.getId() != 0) {
                                            dialog.dismiss();
                                            final ProgressDialog progressDialog = new ProgressDialog(See_More_Article.this,
                                                    R.style.AppTheme_Dark_Dialog);
                                            progressDialog.setIndeterminate(true);
                                            progressDialog.setMessage("Connexion en cours...");
                                            progressDialog.show();
                                            sessionManagerUser.storeUser(user);
                                            new CountDownTimer(3000, 1000) {

                                                @Override
                                                public void onTick(long millisUntilFinished) {

                                                }

                                                @Override
                                                public void onFinish() {
                                                    Intent intent = new Intent(getApplicationContext(), Comment.class);
                                                    intent.putExtra("id_article", id_article);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                                    progressDialog.dismiss();
                                                }
                                            }.start();
                                        } else {
                                            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(See_More_Article.this);
                                            alertDialogBuilder.setTitle("Informations incorrectes");
                                            alertDialogBuilder.setMessage("Il se peut que l'adresse email ou le mot de passe que vous avez saisi soit incorrect(e). Veuillez ré-essayer!");

                                            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                                    alertDialog.hide();
                                                }
                                            });

                                            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();
                                        }
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "L'adresse email n'est pas valide!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Tous les champs doivent être rempli!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
    }
    /*=======================================================================*/
    //La méthode qui retourne l'article
    private Articles get_Article(int id_article){
        Articles article = new Articles();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Articles/search_article.php?id="+id_article+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                article.setId(jsonObject.optInt("id"));
                article.setDate_sent(jsonObject.optString("date_sent"));
                article.setTitre(jsonObject.optString("titre"));
                article.setContent(jsonObject.optString("content"));
                article.setPhoto(jsonObject.optString("photo"));
                article.setSource(jsonObject.optString("source"));
                article.setNom(jsonObject.optString("nom"));
                article.setPrenom(jsonObject.optString("prenom"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return article;
    }
    /*=========================================================================*/
    private Users get_user(String email, String password){
        Users user = new Users();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        BufferedReader bufferedReader = null;
        String result ="";
        URLConnection connection = null;
        String link = "http://marrakech4holidays.com/Carte_Visite_API/Jeunesse_Layenne_API/Users/login.php?email="+email+"&password="+password+"&token="+token;
        try {
            URL url = new URL(link);
            connection = url.openConnection();
            connection.connect();
            bufferedReader = new BufferedReader(new InputStreamReader((InputStream) connection.getContent(),"UTF-8"));
            result = bufferedReader.readLine();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                user.setId(jsonObject.optInt("id"));
                user.setNom(jsonObject.optString("nom"));
                user.setPrenom(jsonObject.optString("prenom"));
                user.setAdresse(jsonObject.optString("adresse"));
                user.setSexe(jsonObject.optString("sexe"));
                user.setTel(jsonObject.optString("tel"));
                user.setEmail(jsonObject.optString("email"));
                user.setPassword(jsonObject.optString("password"));
                user.setVille(jsonObject.optString("ville"));
                user.setCode_postal(jsonObject.optInt("code_postal"));
                user.setProfil(jsonObject.optString("profil"));
                user.setCouverture(jsonObject.optString("couverture"));
                user.setPays(jsonObject.optString("pays"));
                user.setEtat(jsonObject.optString("etat"));
                user.setStatut(jsonObject.optString("statut"));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
