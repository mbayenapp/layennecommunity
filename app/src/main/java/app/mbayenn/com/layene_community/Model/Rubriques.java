package app.mbayenn.com.layene_community.Model;

/**
 * Created by bfali on 03/12/2016.
 */

public class Rubriques {
    private int id;
    private String nom,image;

    public Rubriques() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
